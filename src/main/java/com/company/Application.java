package com.company;

import com.company.service.SpeakerService;
import com.company.service.SpeakerServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {

        // spring will inject the needed depedency via Appconfig
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        // old way
        // SpeakerService service = new SpeakerServiceImpl();
        SpeakerService service = applicationContext.getBean("speakerService",SpeakerService.class);
        System.out.println(service.findAll().get(0).getFirstName());

    }
}
