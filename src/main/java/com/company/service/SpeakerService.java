package com.company.service;

import com.company.model.Speaker;
import com.company.repository.SpeakerRepository;

import java.util.List;

public interface SpeakerService {
    List<Speaker> findAll();
    void setRepository(SpeakerRepository repository);
}
