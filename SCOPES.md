## Scopes
>valid in any configuration
* singleton 
* prototype
>only valid in web aware projects 
* request 
* session 
* global

### Singleton 
will create only one instance per spring container 
```java
@Scope("singleton") // or @Scope(BeanDefination.SCOPE_SINGLTON)
public class My{
    
}
```

### prototype 
will create unique object per request ( like opposite of singleton)

```java
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class My {

}
```
