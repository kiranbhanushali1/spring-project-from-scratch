package com.company;
//applicationContex.xml is replaced by this
//using @Configuration

import com.company.repository.SpeakerRepository;
import com.company.repository.SpeakerRepositoryImpl;
import com.company.service.SpeakerService;
import com.company.service.SpeakerServiceImpl;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.company")
public class AppConfig {

    /*

    // injection
    @Bean(name="speakerService")
    @Scope(value = BeanDefinition.SCOPE_SINGLETON)
    public SpeakerService getSpeakerService(){
        // way 1 :
//        SpeakerService service = new SpeakerServiceImpl();
//        service.setRepository(getSpeakerRepository()); //using setter injection method

        // way 2 : using constructor injection
//        SpeakerService service = new SpeakerServiceImpl(getSpeakerRepository());
//        return service;

        // way 3 : using Autowired  // add annotation @Autowired to the setSpeakerRepository
        SpeakerService service = new SpeakerServiceImpl();
        return service;
    }

    //setter injection
    @Bean(name="speakerRepository")
    public SpeakerRepository getSpeakerRepository(){
        return new SpeakerRepositoryImpl();
    }

     */

}
