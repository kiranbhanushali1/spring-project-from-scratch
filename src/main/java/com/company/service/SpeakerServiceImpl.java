package com.company.service;

import com.company.model.Speaker;
import com.company.repository.SpeakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("speakerService")
public class SpeakerServiceImpl implements SpeakerService {
    // now we are injecting
    private SpeakerRepository repository ;

    public SpeakerServiceImpl() {

    }
    public SpeakerServiceImpl(SpeakerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Speaker> findAll(){
        return repository.findAll();
    }

    @Autowired
    @Override
    public void setRepository(SpeakerRepository repository) {
        this.repository = repository;
    }

}
